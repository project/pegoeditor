<?php
/**
 * @file
 * This file contains administrative functions used only by the administrative pages of PEGOEditor.
 */

function PEGOEditor_settings() {
  if (variable_get('filter_html', 0)) {
    $form['html_area_filtertags'] = array(
      '#type' => 'item',
      '#title' => t('Filter HTML tags'),
      '#value' => t('The Filter HTML Tags is Enabled, This conflicts with htmlarea. ') . l(t('Please disable'), 'admin/system/filters'),
    );
  }

  $form['PEGOEditor_user_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default for New Users'),
   // '#return_value' => true,
    '#default_value' => variable_get('PEGOEditor_user_default', TRUE),
    '#description' => t('Default setting which determines if a new user will be allowed to use PEGOEditor'),
  );
  /* TODO
  $form['PEGOEditor_usestyle'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the themes current style'),
    '#default_value' => variable_get('PEGOEditor_usestyle', 1),
    '#description' => t('Enabling this will instruct PEGOEditor to use the CSS that is being used in your current theme'),
  );
*/

  $form['PEGOEditor_visgroup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Textarea specific visibility settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['PEGOEditor_visgroup']['PEGOEditor_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show Textarea on specific pages'),
    '#default_value' => variable_get('PEGOEditor_visibility', 0),
    '#options' => array(
      t('Show on every page except the listed pages.'),
      t('Show on only the listed pages.')),
  );
  $form['PEGOEditor_visgroup']['PEGOEditor_display'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('PEGOEditor_display', 'admin/*'),
    '#cols' => 40,
    '#rows' => 5,
    '#description' => t("Enter one page per line as Drupal paths. The path to an individual is the Drupal path followed by '.' and the name of the textarea. The '*' character is a wildcard. Example paths are '<em>blog</em>' for the blog page and '<em>blog/*</em>' for every personal blog. '<em>node/*/blog.body</em>' is for the textarea named body on the blog pages. '<em>&lt;front&gt;</em>' is the front page."),
  );

  return system_settings_form($form);
}

function PEGOEditor_settings_toolbar() {
  $toolbar = variable_get("PEGOEditor_toolbar", _PEGOEditor_defaults("options"));

  $form['PEGOEditor_toolbar'] = array('#tree' => TRUE);
  $form['PEGOEditor_toolbar']['#theme'] = 'PEGOEditor_settings_toolbar_form';

  foreach ($toolbar as $k => $v) {

      // display icon

      $form['PEGOEditor_toolbar'][$k]['icon'] = array(
        '#type' => 'hidden',
        '#value' => $v['icon'],
      );
      // display name
      $form['PEGOEditor_toolbar'][$k]['desc'] = array(
        '#type' => 'hidden',
        '#value' => t($v['desc']),
      );
      if ($v['islabel']) {
        $form['PEGOEditor_toolbar'][$k]['data'] = array(
        '#type' => 'textfield',
        '#default_value' => $v['data'],
        '#size' => 20,
        '#maxlength' => 20
        );
      }
      else {
        $form['PEGOEditor_toolbar'][$k]['data'] = array(
        '#type' => 'hidden',
        '#value' => $v['data'],
        );
      }

      $form['PEGOEditor_toolbar'][$k]['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' =>  $v['enabled'],
        '#return_value' => 1,
      );
      $form['PEGOEditor_toolbar'][$k]['islabel'] = array(
        '#type' => 'hidden',
        '#default_value' => $v['islabel']
      );

  }

  return system_settings_form($form);
}

function theme_PEGOEditor_settings_toolbar_form($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    $label='';
    if ($form[$key]['islabel']['#value']==1) {
      $label='';
    }
    else {
      $label=$form[$key]['data']['#value'];
    }

    $row=array();
    $row[] = '<img src="' . base_path() . PEGOEditor_get_jsdir() . '/images/' . $form[$key]['icon']['#value'] . '" alt="" />' . drupal_render($form[$key]['icon']);
    $row[] = $label . drupal_render($form[$key]['data']);
    $row[] = $form[$key]['desc']['#value'] . drupal_render($form[$key]['desc']);
    $row[] = drupal_render($form[$key]['enabled']) . drupal_render($form[$key]['islabel']);
    $rows[] = $row;
  }

  $header = array(t('icon'), t('name'), t('description'), t('enabled'));
  $output .= theme('table', $header, $rows);

  return $output;
}

function PEGOEditor_settings_plugins() {
  // build list of plugins
  $files = array_merge(file_scan_directory(PEGOEditor_get_jsdir() . '/plugins', '.*', array('.', '..', 'CVS', '.svn'), 0, FALSE), file_scan_directory(drupal_get_path('module', 'PEGOEditor') . '/plugins', '.*', array('.', '..', 'CVS', '.svn'), 0, FALSE));
  usort($files, '_PEGOEditor_sort_plugins');
  $plugins = variable_get('PEGOEditor_plugins', array());

  $form['PEGOEditor_plugins'] = array('#tree' => TRUE);
  $form['PEGOEditor_plugins']['#theme'] = 'PEGOEditor_settings_plugins_form';
  foreach ($files as $f) {
    // display name
    $form['PEGOEditor_plugins'][$f->filename]['desc'] = array(
      '#type' => 'item',
      '#value' => $f->basename,
    );
    // name saved with the enable setting
    $form['PEGOEditor_plugins'][$f->filename]['name'] = array(
      '#type' => 'value',
      '#value' => $f->basename,
    );
    // enable checkbox
    $form['PEGOEditor_plugins'][$f->filename]['enable'] = array(
      '#type' => 'checkbox',
      '#default_value' => $plugins[$f->filename]['enable'],
      '#return_value' => 1,
    );
  }

  return system_settings_form($form);
}

/**
 * Format the pluigin settings page into a table
 */
function theme_PEGOEditor_settings_plugins_form($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['desc'])) {
      $row[] = drupal_render($form[$key]['desc']) . drupal_render($form[$key]['name']);
      $row[] = drupal_render($form[$key]['enable']);
    }
    $rows[] = $row;
  }
  $header = array(t("plugin"), t("enable"));
  $output = theme('table', $header, $rows);

  return $output;
}


function PEGOEditor_settings_custom() {
  $form['PEGOEditor_customjs'] = array(
    '#type' => 'textarea',
    '#title' => t('custom javascript'),
    '#default_value' => variable_get('PEGOEditor_customjs', ''),
    '#cols' => 80,
    '#rows' => 15,
    '#description' => t('Enter any javascript that you wish execute before the PEGOEditor is inited.'),
  );
  return system_settings_form($form);
}

function PEGOEditor_settings_styles() {
  $styles=variable_get('PEGOEditor_styles', array());

  $form['PEGOEditor_styles'] = array();
  $form['PEGOEditor_styles'] = array('#tree' => TRUE);
  $form['PEGOEditor_styles']['public'] = array(
    '#type' => 'textfield',
    '#title' => t('Public stylesheets'),
    '#default_value' => $styles['public'],
    '#size' => 100,
    '#maxlength' => 300,
    '#description' => t('Public stylesheets classes can be assigned to the page elements by the editor user.'),
  );
  $form['PEGOEditor_styles']['private'] = array(
    '#type' => 'textfield',
    '#title' => t('Private stylesheets'),
    '#default_value' => $styles['private'],
    '#size' => 100,
    '#maxlength' => 300,
    '#description' => t('Private stylesheets applies to the page but are not visible to the user.'),
  );
  $form['PEGOEditor_styles']['freestyle'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow embedded styles'),
      '#default_value' => $styles['freestyle'],
      '#return_value' => 1,
      '#description' => t('Allows the user to assign embedded style properties.'),
    );
  return system_settings_form($form);
}

function PEGOEditor_settings_styles_submit($form, &$form_state) {
  system_settings_form_submit($form, $form_state);
  $form_state['redirect'] = 'admin/settings/PEGOEditor/styles';
}

//---------//

function _PEGOEditor_sort($a, $b) {
  return ($a['weight'] > $b['weight'] ? 1 : ($a['weight'] < $b['weight'] ? -1 : (strcmp($a['data'], $b['data']))));
}

function _PEGOEditor_sort_plugins($a, $b) {
  return strcmp($a->basename, $b->basename);
}


/**
 * Replace all textarea elements with attribute "PEGOEditor" with an "open editor" button
 * Since it's done through Javascript, if Javascript is disabled it does nothing (that is exactly what we want it to do - keep the plain textarea).
 */

if (Drupal.jsEnabled) {
  $(document).ready(
    function () {
      var editors = [];
      $('textarea[@editor="PEGOEditor"]').each(
        function () {
          editors[editors.length] = this.id;
        }
      );
      
      //Evalutaion of any custom function inserted by the administrator
      if(Drupal.settings.PEGOEditor.custom) eval(Drupal.settings.PEGOEditor.custom);
                 
      //Put somewhere in a template?
      var editorCode='' +
      		'<div class="open_editor_div">' +
      		'<a href="#" title="@OPEN_EDITOR_TEXT@" onclick="@OPEN_EDITOR_ACTION@" class="open_editor_link">@OPEN_EDITOR_TEXT@</a>' +
      		'<a HREF="#" title="@EDIT_PLAIN_TEXT@" onclick="@EDIT_PLAIN_ACTION@" class="edit_plain_link">@EDIT_PLAIN_TEXT@</a>' +
      		'</div>';
      	
      editorCode=editorCode.replace(/@OPEN_EDITOR_TEXT@/g,Drupal.settings.PEGOEditor.open_link_text);
      editorCode=editorCode.replace(/@EDIT_PLAIN_TEXT@/g,Drupal.settings.PEGOEditor.edit_plain_link_text);
      
      for(var i in editors)
      {
      	var textarea=document.getElementById(editors[i]);
      	var tmp=editorCode;
      	
      	tmp=tmp.replace("@OPEN_EDITOR_ACTION@","PopupEditor.openPopupEditor('"+Drupal.settings.PEGOEditor.path+"/popupEditor.htm','"+editors[i]+"','"+editors[i]+"'); return false;");
      	tmp=tmp.replace("@EDIT_PLAIN_ACTION@","document.getElementById('"+textarea.id+"').style.display=''; return false;");
      	
      	var div=document.createElement("div");
      	div.innerHTML=tmp;      	
      	var myedit=div.firstChild;
      	
      	textarea.parentNode.insertBefore(myedit, textarea);      	
      	textarea.style.display="none";      	
      	
      }
    }
    
  );
}

function PopupEditor()
{}

PopupEditor.openPopupEditor=function(popupeditor_url,textInputId,textOutputId)
{
	var textInput=document.getElementById(textInputId);
	if(!textInput) 
	{
		alert("[Error - openPopupEditor]\nMissing input text field.");
		return;
	}
	
	init=new Object();
	init["text"]=textInput.innerHTML;
	init["plugins"]=Drupal.settings.PEGOEditor.plugins;
	init["toolbar"]=[];
	for(var i in Drupal.settings.PEGOEditor.toolbar)
	{
		init["toolbar"][0]=Drupal.settings.PEGOEditor.toolbar[i];
	}
	init["lang"]=Drupal.settings.PEGOEditor.lang;
	init["drupal_base_url"]=Drupal.settings.PEGOEditor.base;
	init["node_link"]="?q=node";	
	init["public_styles"]=Drupal.settings.PEGOEditor.public_styles;
	init["private_styles"]=Drupal.settings.PEGOEditor.private_styles;
	init["freestyle"]=Drupal.settings.PEGOEditor.freestyle;
	init["help_url"]="http://www.editoraccessibile.org/?q=manuale";
	switch(Drupal.settings.PEGOEditor.lang)
	{
	case "it":
		init["help_url"]="http://www.editoraccessibile.org/?q=manuale";
		break;
	case "en":
		init["help_url"]="http://www.accessibleeditor.org/?q=manual";
		break;
	}
	
	var action=function(param) 
	{
		if(textInputId!=textOutputId) textInput.innerHTML=param["text"];		
    	if(anastasis.is_ie)
    		document.getElementById(textOutputId).value=param["text"];
    	else
    	{
    		document.getElementById(textOutputId).value=param["text"];
    		document.getElementById(textOutputId).innerHTML=param["text"];
    	}
	}
		

	anastasis.openDialog(popupeditor_url, action, init);
}


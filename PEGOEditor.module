<?php
/**
 * @file
 * Main PEGOEditor files. Allows Drupal to use the wysiwyg editor PEGOEditor to replace textarea fields through a popup window.
 *
 * @defgroup PEGOEditor PEGOEditor: Allows Drupal to use the wysiwyg editor PEGOEditor to replace textarea fields through a popup window.
 */

/**
 * Implementation of hook_help().
 */
function PEGOEditor_help($path, $arg) {
  switch ($path) {
    case "admin/settings/PEGOEditor":
      if (!PEGOEditor_get_jsdir()) {
        $output = t('<strong>PEGOEditor core Javascript is not installed,please download it from <a href="http://www.accessibleeditor.org">http://www.accessibleeditor.org</a> and install it into the PEGOEditor module directory.</strong>');
      }
      break;
    case "admin/settings/PEGOEditor/toolbar":
      $output = t('Select toolbar elements to be displayed in the Editor.<br />');
      break;
    case "admin/settings/PEGOEditor/styles":
      $output = t('Set the path of the stylesheets loaded inside the editor.Separate each file name with a , (comma).<br />'.
          'Classes from <em>public</em> stylesheets will be available to the user (if you enable the <strong>CCSHandler</strong> module). <br />'.
          'Use <em>%current_theme%</em> to denote the current theme folder.'
          );
      break;
  }

  return $output;
}

/**
 * Implementation of hook_theme().
 */
function PEGOEditor_theme($existing = NULL, $type = NULL, $theme = NULL, $path = NULL) {
  return array(
    'PEGOEditor_settings_toolbar_form' => array('arguments' => array('form')),
    'PEGOEditor_settings_plugins_form' => array('arguments' => array('form')),
  );
}

/**
 * Implementation of hook_menu().
 */
function PEGOEditor_menu() {
  $items = array();

  $items['admin/settings/PEGOEditor'] = array(
    'title' => 'PEGOEditor',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('PEGOEditor_settings'),
    'access arguments' => array('administer PEGOEditor'),
    'description' => 'Configure PEGOEditor',
    'file' => 'PEGOEditor.admin.inc'
  );
  $items['admin/settings/PEGOEditor/main'] = array(
    'title' => 'general',
    'page callback' => 'PEGOEditor_settings',
    'access arguments' => array('administer PEGOEditor'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
    'file' => 'PEGOEditor.admin.inc'
  );
  $items['admin/settings/PEGOEditor/toolbar'] = array(
    'title' => 'toolbar',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('PEGOEditor_settings_toolbar'),
    'access arguments' => array('administer PEGOEditor'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -7,
    'file' => 'PEGOEditor.admin.inc'
  );
  $items['admin/settings/PEGOEditor/plugins'] = array(
    'title' => 'plugins',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('PEGOEditor_settings_plugins'),
    'access arguments' => array('administer PEGOEditor'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'PEGOEditor.admin.inc',
    'weight' => -4,
  );
  $items['admin/settings/PEGOEditor/plugins/select'] = array(
    'title' => 'select',
    'page callback' => 'PEGOEditor_settings_plugins',
    'access arguments' => array('administer PEGOEditor'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
    'file' => 'PEGOEditor.admin.inc'
  );
  $items['admin/settings/PEGOEditor/custom'] = array(
    'title' => 'custom js',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('PEGOEditor_settings_custom'),
    'access arguments' => array('administer PEGOEditor'),
    'weight' => -1,
    'type' => MENU_LOCAL_TASK,
    'file' => 'PEGOEditor.admin.inc'
  );
  $items['admin/settings/PEGOEditor/styles'] = array(
    'title' => 'stylesheets',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('PEGOEditor_settings_styles'),
    'access arguments' => array('administer PEGOEditor'),
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
    'file' => 'PEGOEditor.admin.inc'
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function PEGOEditor_perm() {
  return array('administer PEGOEditor', 'upload images');
}

/**
 * Implementation of hook_elements().
 */
function PEGOEditor_elements() {
  $elements['textarea'] = array('#after_build' => array('PEGOEditor_after_build'));
  return $elements;
}

function PEGOEditor_after_build($form, $form_element) {
  static $loaded = FALSE;
  if (_PEGOEditor_is_changed($form['#name'])) {
    $form['#attributes']['editor'] = 'PEGOEditor';
    //$form['#resizable'] = FALSE;
    _PEGOEditor_load_editor();
  }
  return $form;
}

/**
 * Implementation of hook_user().
 */
function PEGOEditor_user($type, $edit, &$user, $category = NULL) {
  switch ($type) {
    case "form":
      if ($category == 'account') {
        $form['htmlarea'] = array(
          '#type' => 'fieldset',
          '#title' => t('text area settings')
          );
        $form['htmlarea']['PEGOEditor_isenabled'] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable rich text editor while authoring'),
          '#return_value' => 1,
          '#default_value' => isset($user->PEGOEditor_isenabled) ? $user->PEGOEditor_isenabled : 1,
        );
        return $form;
      }
      return NULL;
    case 'validate':
      return array('PEGOEditor_isenabled' => $edit['PEGOEditor_isenabled']);
  }
}



/*
 * Internal Functions
 */

/**
 * find where the js has been put.
 */
function PEGOEditor_get_jsdir() {
  static $path = FALSE;

  if (!$path && ($files = file_scan_directory(drupal_get_path('module', 'PEGOEditor'), '^(htmlarea.js)$', array('.', '..', 'CVS', '.svn'), 0, TRUE, 'name', 1))) {
    if (isset($files['htmlarea'])) {
      $path = dirname($files['htmlarea']->filename);
    }
  }
  return $path;
}

function _PEGOEditor_load_editor() {
  static $loaded = FALSE;

  drupal_add_js(drupal_get_path('module', 'PEGOEditor') .'/runPEGOEditor.js', 'module');
  drupal_add_js(drupal_get_path('module', 'PEGOEditor') .'/PEGOEditor/anastasis/Anastasis.js', 'module');

  if (!$loaded) {
    drupal_add_css(drupal_get_path('module', 'PEGOEditor') .'/PEGOEditor_style.css');

    $styles=variable_get('PEGOEditor_styles', array());
    $public_styles='[]';
    $private_styles='[]';
    if ($styles['public']) {
      $st=split(',', $styles['public']);
      if (count($st)>0) {
        $public_styles='[';
        foreach ( $st as $key => $value ) {
          $public_styles .= '"' . str_replace("%current_theme%", base_path() . path_to_theme(), $value) . '",';
        }
        $public_styles=preg_replace('/,$/', '', $public_styles);
        $public_styles .= ']';
      }
    }
    if ($styles['private']) {
      $st=split(',', $styles['private']);
      if (count($st)>0) {
        $private_styles='[';
        foreach ( $st as $key => $value ) {
          $private_styles .= '"' . str_replace("%current_theme%", base_path() . path_to_theme(), $value) . '",';
        }
        $private_styles=preg_replace('/,$/', '', $private_styles);
        $private_styles .= ']';
      }
    }

    $settings['PEGOEditor'] = array(
      'base' => base_path(),
      'path' => base_path() . PEGOEditor_get_jsdir(),
      'open_link_text' => t('Open editor'),
      'edit_plain_link_text' => t('Edit plain text'),
      'toolbar' => array(),
      'lang' => _PEGOEditor_lang(),
      'public_styles' => $public_styles,
      'private_styles' => $private_styles,
      'freestyle' => $styles['freestyle'] ? TRUE : FALSE
    );

  function filter_toolbar($a) {return $a['enabled'];}

    $toolbar = variable_get("PEGOEditor_toolbar", _PEGOEditor_defaults("options"));
    $toolbar = array_filter($toolbar, 'filter_toolbar');

    $buttons = array();
    foreach ($toolbar as $button) {
      $buttons[] = $button['data'];
    }

    $settings['PEGOEditor']['toolbar'][] = $buttons;

    if ($js = variable_get('PEGOEditor_customjs', NULL)) {
      $settings['PEGOEditor']['custom'] = $js;
    }

    drupal_add_js(drupal_get_path('module', 'PEGOEditor') .'/htmlarea.js', 'module');

    if ($plugin_list = variable_get('PEGOEditor_plugins', NULL)) {
      $plugins = array();
      foreach ($plugin_list as $d => $a) {
        if ($a['enable']) {
          $plugins[] = $a['name'];
          $plugin = drupal_strtolower(preg_replace_callback('/([a-z])([A-Z])([a-z])/', '_PEGOEditor_plugin_preg', $a['name']));
        }
      }
    }
    if (isset($plugins)) {
      $settings['PEGOEditor']['plugins'] = $plugins;
    }
    drupal_add_js($settings, 'setting');
    $loaded = TRUE;
  }
}

function _PEGOEditor_defaults($field) {
  global $htmlarea_codeview, $base_url;
  switch ($field) {
    case "options":
      return array(
        array("data" => "Standard", "desc" => "Label for the first buttons row", "enabled" => "1", "icon" => "", "islabel" => "1"),
        array("data" => "anchor", "enabled" => "1", "icon" => ""),
        array("data" => "save", "desc" => "Save", "enabled" => "1", "icon" => "save.gif"),
        array("data" => "cancel", "desc" => "Cancel", "enabled" => "1", "icon" => "cancel.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "undo", "desc" => "Undo last action", "enabled" => "1", "icon" => "undo.gif"),
        array("data" => "redo", "desc" => "Redo last cancelled action", "enabled" => "1", "icon" => "redo.gif"),
        array("data" => "findreplace", "desc" => "Find/Replace", "enabled" => "1", "icon" => "replace.gif"),
        array("data" => "pasteunformatted", "desc" => "Paste unformatted text", "enabled" => "1", "icon" => "paste_unformatted.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "bold", "desc" => "Bold", "enabled" => "1", "icon" => "format_bold.gif"),
        array("data" => "italic", "desc" => "Italic", "enabled" => "1", "icon" => "format_italic.gif"),
        array("data" => "underline", "desc" => "Underline", "enabled" => "1", "icon" => "format_underline.gif"),
        array("data" => "strikethrough", "desc" => "Strikethrough", "enabled" => "1", "icon" => "format_strike.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "lefttoright", "desc" => "Read left to right", "enabled" => "0", "icon" => "left_to_right.gif"),
        array("data" => "righttoleft", "desc" => "Read right to left", "enabled" => "0", "icon" => "right_to_left.gif"),
        array("data" => "separator", "enabled" => "0", "icon" => ""),
        array("data" => "subscript", "desc" => "Subscript", "enabled" => "1", "icon" => "format_sub.gif"),
        array("data" => "superscript", "desc" => "Superscript", "enabled" => "1", "icon" => "format_sup.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "justifyleft", "desc" => "Align left", "enabled" => "1", "icon" => "align_left.gif"),
        array("data" => "justifycenter", "desc" => "Center text", "enabled" => "1", "icon" => "align_center.gif"),
        array("data" => "justifyright", "desc" => "Align right", "enabled" => "1", "icon" => "align_right.gif"),
        array("data" => "justifyfull", "desc" => "Justify", "enabled" => "1", "icon" => "align_justify.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "insertorderedlist", "desc" => "Insert ordered list", "enabled" => "1", "icon" => "list_num.gif"),
        array("data" => "insertunorderedlist", "desc" => "Insert unoredered list", "enabled" => "1", "icon" => "list_bullet.gif"),
        array("data" => "outdent", "desc" => "Outdent text", "enabled" => "1", "icon" => "indent_less.gif"),
        array("data" => "indent", "desc" => "Indent text", "enabled" => "1", "icon" => "indent_more.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "forecolor", "desc" => "Text color", "enabled" => "1", "icon" => "color_fg.gif"),
        array("data" => "hilitecolor", "desc" => "Text background color", "enabled" => "1", "icon" => "color_bg.gif"),
        array("data" => "linebreak", "desc" => "New button row", "enabled" => "1", "icon" => ""),
        array("data" => "HTML", "desc" => "Label for the second buttons row", "enabled" => "1", "icon" => "", "islabel" => "1"),
        array("data" => "anchor", "enabled" => "1", "icon" => ""),
        array("data" => "formatblock", "desc" => "Assign HTML block tag", "enabled" => "1", "icon" => ""),
        array("data" => "space", "enabled" => "1", "icon" => ""),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "inserthorizontalrule", "desc" => "Insert horizontal rule", "enabled" => "1", "icon" => "hr.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "createlink", "desc" => "Create external link", "enabled" => "1", "icon" => "link.gif"),
        array("data" => "insertanchor", "desc" => "Insert anchor", "enabled" => "1", "icon" => "insert_anchor.gif"),
        array("data" => "nodelink", "desc" => "Create internal link (node link)", "enabled" => "1", "icon" => "node_link.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "inserttable", "desc" => "Insert table", "enabled" => "1", "icon" => "insert_table.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "insertimage", "desc" => "Insert image", "enabled" => "1", "icon" => "image.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "changelang", "desc" => "Specify text language", "enabled" => "1", "icon" => "changelang.gif"),
        array("data" => "space", "enabled" => "1", "icon" => ""),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "deleteall", "desc" => "Delete all", "enabled" => "1", "icon" => "delete_all.gif"),
        array("data" => "clearstyle", "desc" => "Clear all embedded styles", "enabled" => "1", "icon" => "clear_tags.gif"),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "space", "enabled" => "1", "icon" => ""),
        array("data" => "htmlmode", "desc" => "Edit html code", "enabled" => "1", "icon" => "html.gif"),
        array("data" => "space", "enabled" => "1", "icon" => ""),
        array("data" => "separator", "enabled" => "1", "icon" => ""),
        array("data" => "space", "enabled" => "1", "icon" => ""),
        array("data" => "showhelp", "desc" => "Help", "enabled" => "1", "icon" => "help.gif"),
        array("data" => "about", "desc" => "About", "enabled" => "1", "icon" => "about.gif"));
      break;
    }
}

function _PEGOEditor_is_changed($name = '') {

  global $user;
  static $textarea;

  if (PEGOEditor_unsopported_browser() || (isset($user->htmlarea_isenabled) && $user->htmlarea_isenabled == 0) || (!isset($user->htmlarea_isenabled) && !variable_get("htmlarea_user_default", TRUE)) || !PEGOEditor_get_jsdir()) {
    return FALSE;
  }
  $path = drupal_get_path_alias($_GET['q']) .'.';
  if ($name != '') {
    $ret = drupal_match_path($path, variable_get('PEGOEditor_display', "admin/*"));
    if ($path != $_GET['q']) {
      $ret = $page_match || drupal_match_path($_GET['q'], variable_get('PEGOEditor_display', "admin/*"));
    }
    $ret=($ret or drupal_match_path($path . $name, variable_get('PEGOEditor_display', "admin/*")));

    $ret = !(variable_get('PEGOEditor_visibility', 0) xor $ret);

    if ($ret) {
      $textarea = TRUE;
    }
    return $ret;
  }
  else {
    return $textarea ? TRUE : FALSE;
  }
}

function _PEGOEditor_lang() {
  global $user;
  if (module_exists('locale')) {
    if ($user->language) {
      if (file_exists(PEGOEditor_get_jsdir() . "/lang/$user->language.js")) {
        return $user->language;
      }
    }
    elseif (module_exists('i18n')) {
      $ulang = _i18n_get_lang();
      if (file_exists(PEGOEditor_get_jsdir() . "/lang/$ulang.js")) {
        return $ulang;
      }
    }
    else {
      $ulang = key(locale_language_list('name'));
      if (file_exists(PEGOEditor_get_jsdir() . "/lang/$ulang.js")) {
        return $ulang;
      }
    }
  }
  return 'en';
}

function _PEGOEditor_plugin_preg($m) {
  return $m[1] . '-' . $m[2] . $m[3];
}


function PEGOEditor_unsopported_browser() {

  if (strpos($_SERVER['HTTP_USER_AGENT'], "Safari")) {
    return TRUE;
  }
  
  if (strpos($_SERVER['HTTP_USER_AGENT'], "Opera")) {
    return TRUE;
  }

  if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE 5.0")) {
    return TRUE;
  }

  return FALSE;
}
